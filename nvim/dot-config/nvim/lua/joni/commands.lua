local function go_mod_tidy()
    vim.cmd([[ ! go mod tidy ]])
    vim.cmd([[ LspRestart ]])
end
vim.api.nvim_create_user_command('GoModTidy', go_mod_tidy, {})

local function go_fmt_all()
    vim.cmd([[ ! go fmt ./... ]])
end
vim.api.nvim_create_user_command('GoFmtAll', go_fmt_all, {})
