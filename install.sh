#!/usr/bin/env bash

set -e

SCRIPT=$(readlink -f "$0")
DIRNAME=$(dirname "$SCRIPT")
HOSTNAME=$(hostname)

NEEDED_PATHS=(
    "$HOME"/.config
    "$HOME"/.local/bin
    "$HOME"/.local/share/applications/
    "$HOME"/.local/share/icons/hicolor/256x256/apps
)

ensure_env() {
    echo "Ensuring environment is sane"
    for path in "${NEEDED_PATHS[@]}"; do
        if [ ! -d "$path" ]; then
            echo "==> Creating missing needed directory $path"
            mkdir -p "$path"
        fi
    done
    echo ""
}

CONFIGS=(
    bin
    gf2
    git
    kitty
    mpv
    nvim
    profile
    sxiv
    tmux
    x-resources
    zathura
    zsh
)


stow_all() {
    stowdir=$1

    echo "Stowing configurations from $stowdir"
    for config in "${CONFIGS[@]}"; do
        echo ""
        echo "==> Stowing $config"
        if [ -d "${config}-${HOSTNAME}" ]; then
            config="$config"-"$HOSTNAME"
            echo "    Found override for host $HOSTNAME. Using $config instead"
        fi

        stow --verbose --restow --target="$HOME" --dir="$stowdir" --dotfiles "$config"
    done
    echo ""
}

set_applications() {
   echo "Installing .desktop files"
   for application in "$DIRNAME"/applications/* ; do
       if [ -f "$application" ]; then
           case $application in *.desktop)
               install --verbose "$application" "$HOME"/.local/share/applications/"$(basename "$application")";;
           esac
       fi
   done
   echo ""
}

ensure_env
stow_all "$DIRNAME"
set_applications
