# Notes

## OCaml

- Installing OCaml

```sh
sudo pacman -S opam
```

- Initialize

```sh
opam init --bare -a -y
```

- Create default switch with usefull packages

```sh
opam switch create default ocaml-base-compiler.{version}
opam install utop ocaml-lsp-server ocamlformat
```

- Create switch for directory

```sh
opam switch create . {name} ocaml-base-compile.{version}
```

- Update switches compiler version

```sh
opam install ocam-base-compiler.{version} --update-invariant
```
